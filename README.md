NYL-Export-Policy-Rule-Role
=========

This role utilizes the NetApp API to create export policy rule. 
The latest documentaion on the Ansible/NetApp API used by this role can be found at: 
https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_export_policy_rule_module.html

Requirements
------------
This role requires:

1. Python, Version 3.6.9
2. netapp.ontap, Version 21.10.0
3. netapp-lib, Version 2021.6.25

Role Variables
--------------
(Required)

*netapp_hostname:*

   - This variable should be set to the NetApp hostname that is used to manage the export policy
   
*netapp_username:*

   - This variable should be set to the NetApp username that is used to manage the export policy
   
*netapp_password:*

   - This variable should be set to the NetApp password that is used to manage the export policy

*netapp_svm:*

   - This variable will set the VServer that will be used to manage the export policy

*export_policy_name:*
   
   - This variable will be used to name the export policy

*policy_rule_index:*
   
   - index of the export policy rule

*policy_client_match:*
   
   - List of Client Match host names, IP Addresses, Netgroups, or Domains. If rule_index is not provided, client_match is used as a key to fetch current rule to determine create,delete,modify actions. If a rule with provided client_match exists, a new rule will not be created, but the existing rule will be modified or deleted. If a rule with provided client_match doesn't exist, a new rule will be created if state is present.
  
*policy_ro_rule:*
   
   - List of Read only access specifications for the rule (Choices: any, none , never, krb5, krb5i, krb5p, ntlm,and sys)

*policy_rw_rule:*
   
   - List of Read Write access specifications for the rule (Choices: any, none , never, krb5, krb5i, krb5p, ntlm,and sys)

(Optional)

*protocol_http_port:*

   - Override the default port (80 or 443) with this port
   
*protocol_ontapi:*

   - The ontap api version to use

*protocol_use_rest:*

   - REST API if supported by the target system for all the resources and attributes the module requires. Otherwise will revert to ZAPI.

*protocol_validate_certs:*
   
   - If set to no, the SSL certificates will not be validated. This should only set to False used on personally controlled sites using self-signed certificates.

*protocol_cert_filepath:*
   
   - path to SSL client cert file (.pem).

*protocol_key_filepath:*
   
   - path to SSL client key file.
  
*export_rule_su_security:*
   
   - List of Read Write access specifications for the rule

*export_rule_anonymous_id:*
   
   - User name or ID to which anonymous users are mapped. Default value is '65534'.
Example Playbook
----------------

---
- name: Create policy rule
  hosts:
    - all
  gather_facts: false
  become: false
  vars_files:
    - vars/ontap_creds.yml
    - vars/main.yml
  include_roles:
    - nyl-export-policy-rule-role



Author Information
------------------

This role was developed by Enterprise Vision Technologies (*www.evtcorp.com*)